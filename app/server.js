'use strict';

// Load environment.
require('dotenv').config({ path: './config/dev.env' });

// Connect database.
require('./data/db/mongodb');

const app = require('express')();
const bodyParser = require('body-parser');
const port = process.env.PORT;
const errors = require('./utilities/errors');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Implement routing.
require('./routes/index')(app);

// Handle invalid urls.
app.use((req, res, next) => {
  res.status(404).json({       
    statusCode: 404,
    message: 'Requested url not found.'
  });
});

// Handle errors.
app.use((err, req, res, next) => {
  let error = errors.handle(err);
  return res.status(error.statusCode).json(error);
});

// Start server.
app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});