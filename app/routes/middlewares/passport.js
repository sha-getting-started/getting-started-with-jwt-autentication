'use strict';

const passport = require('passport');
const { ExtractJwt, Strategy } = require('passport-jwt');
const usersRepo = require('../../data/repos/users');

const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.SECRET_KEY,
  passReqToCallback: true
}

passport.use('jwt', new Strategy(options, async (req, payload, next) => {
  try {
    // Get user from database.
    let user = await usersRepo.getById(payload._id);
    req.decoded = payload;

    if(user) {
      return next(null, user);
    }

    return next(null, false);
  } catch(err) {
    return next(err, false);
  }
}));

module.exports = passport;