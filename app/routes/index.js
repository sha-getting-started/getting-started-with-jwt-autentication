const fs = require('fs');

/*
* Require all routes inside the controllers folder.
*/
let requireFiles = (directory, app) => {
  fs.readdirSync(directory).forEach(async (file) => {
    if(file === 'index.js') return;
    app.use('/api/v1', require(directory + '/' + file)());
  });
}

module.exports = (app) => {
  requireFiles(__dirname + '/controllers', app);
}