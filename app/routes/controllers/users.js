'use strict';

const express = require('express');
const usersRepo = require('../../data/repos/users');
const authenticate = require('../middlewares/authenticate');

module.exports = function() {
  let router = express.Router();

  router.post('/users/signup', async (req, res, next) => {
    try {
      let results = await usersRepo.signup(req.body);
      return res.status(201).json(results);
    } catch(err) {
      next(err);
    }
  });

  router.post('/users/signin', async (req, res, next) => {
    try {
      let results = await usersRepo.signin(req.body);
      return res.status(200).json(results);
    } catch(err) {
      next(err);
    }
  });

  router.get('/users', authenticate(), async (req, res, next) => {
    try {
      let results = await usersRepo.get();
      return res.status(200).json(results);
    } catch(err) {
      next(err);
    }
  });

  router.get('/users/:userId', async (req, res, next) => {
    try {
      let results = await usersRepo.getById(req.params.userId);
      return res.status(200).json(results);
    } catch(err) {
      next(err);
    }
  });

  return router;
}