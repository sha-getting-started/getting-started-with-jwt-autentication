'use strict';

const bluebird = require('bluebird');
const mongoose = require('mongoose');

// Options for deprecated methods.
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

// Promisify all mongoose functions.
// No need to use the prefix Async.
mongoose.Promise = bluebird;

mongoose.connect(process.env.MONGODB_URI, { 
  useNewUrlParser: true
});

let db = mongoose.connection;

// Handle successful connection.
db.on('connected', () => {
  console.log('MongoDb connected.');
});

// Handle error.
db.on('error', (err) => {
  console.log('Unable to connect MongoDb.', err);
});