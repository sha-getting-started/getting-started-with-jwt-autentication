'use strict';

const jwt = require('jsonwebtoken');

// Users collection.
let User = require('../models/user');

const signup = async (user) => {
  user = new User(user);
  user = await user.save();
  user = user.toJSON();
  delete user.password;
  return user;
}

const signin = async (credentials) => {
  let user = await User.findOne({ _id: credentials._id });

  // Return error if user not found.
  if(!user) {
    return Promise.reject({
      message: 'User not found.',
      statusCode: 404
    });
  }

  // Return error if credentials don't match.
  if(!user.checkPassword(credentials.password)) {
    return Promise.reject({
      message: 'Invalid password.',
      statusCode: 401
    });
  }

  // Create token.
  let payload = { _id: user._id }
  let key = process.env.SECRET_KEY;
  let options = {
    expiresIn: parseInt(process.env.TOKEN_EXPIRY)
  }

  return {
    token: jwt.sign(payload, key, options),
    expiresIn: options.expiresIn
  }
} 

const get = async () => {
  let users =  await User.find({ }, { password: 0 });
  return users;
}

const getById = async (userId) => {
  let user = await User.findOne({ _id: userId }, { password: 0 });

  // Return error if user not found.
  if(!user) {
    return Promise.reject({
      message: 'User not found.',
      statusCode: 404
    });
  }

  return user;
}

module.exports = {
  signup,
  signin,
  get,
  getById
}