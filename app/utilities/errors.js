'use strict';

const handle = (err) => {
  // Log error.
  if(process.env.NODE_ENV !== 'production') {
    console.log(err);
  }
  
  // MongoDb duplicate key error.
  if(err.code == 11000) {
    return {
      statusCode: 409,
      message: 'Duplicate Key error.'
    }
  }

  // Passport invalid token error.
  if(err.name && err.name === 'JsonWebTokenError') {
    return {
      statusCode: 401,
      message: 'Invalid token signature.'
    }
  }

  // Passport token not found error.
  if(err.toString().startsWith('Error')) {
    return {
      statusCode: 401,
      message: 'No authorization token provided'
    }
  }

  // default case.
  return {
    statusCode: 500,
    message: 'Internal server error.'
  }
}

module.exports = {
  handle
}